package gyurme.colourmemory;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import gyurme.colourmemory.Model.Card;
import gyurme.colourmemory.Model.Game;

public class GameTest {

    private Game game;

    @Before
    public void setup() {
        game = new Game();
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(1, Card.CardState.HIDDEN));
        cards.add(new Card(2, Card.CardState.HIDDEN));
        cards.add(new Card(3, Card.CardState.HIDDEN));
        cards.add(new Card(2, Card.CardState.HIDDEN));
        cards.add(new Card(1, Card.CardState.HIDDEN));
        cards.add(new Card(3, Card.CardState.HIDDEN));
        HashMap<Integer, Integer> pairs = game.generatePairs(cards);
        game.setCards(cards);
        game.setPairs(pairs);
    }

    @Test
    public void pairsAreCorrectOnceGenerated() {
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(1, Card.CardState.HIDDEN));
        cards.add(new Card(2, Card.CardState.HIDDEN));
        cards.add(new Card(3, Card.CardState.HIDDEN));
        cards.add(new Card(2, Card.CardState.HIDDEN));
        cards.add(new Card(1, Card.CardState.HIDDEN));
        cards.add(new Card(3, Card.CardState.HIDDEN));
        HashMap<Integer, Integer> pairs = game.generatePairs(cards);
        assert (pairs.get(0) == 4 && pairs.get(4) == 0);
        assert (pairs.get(1) == 3 && pairs.get(3) == 1);
        assert (pairs.get(2) == 5 && pairs.get(5) == 2);
    }

    @Test
    public void gameStateRemainsUnchangedIfMatchFails() {
        boolean matched = game.matchCards(1, 2);
        assert (!matched);
        assert (game.getPairs().containsKey(1));
        assert (game.getPairs().containsKey(2));
        assert (game.getCards().get(1).getCardState() == Card.CardState.HIDDEN);
        assert (game.getCards().get(2).getCardState() == Card.CardState.HIDDEN);
    }

    @Test
    public void gameStateUpdatesIfMatchSucceeds() {
        boolean matched = game.matchCards(0, 4);
        assert (matched);
        assert (!game.getPairs().containsKey(0));
        assert (!game.getPairs().containsKey(4));
        assert (game.getCards().get(0).getCardState() == Card.CardState.MATCHED);
        assert (game.getCards().get(4).getCardState() == Card.CardState.MATCHED);
    }

    @Test
    public void gameEndsIfAllMatchesSucceed() {
        game.matchCards(0, 4);
        game.matchCards(1, 3);
        game.matchCards(2, 5);
        assert (game.hasGameEnded());

    }
}