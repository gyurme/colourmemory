package gyurme.colourmemory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;

import gyurme.colourmemory.Model.Card;
import gyurme.colourmemory.Model.Game;
import gyurme.colourmemory.Model.Player;
import gyurme.colourmemory.Presenter.GamePresenter;
import gyurme.colourmemory.View.GameView;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class GamePresenterTest {
    @Mock
    private GameView view;

    private GamePresenter gamePresenter;

    public Game setUpGame() {
        Game game = new Game();
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(1, Card.CardState.HIDDEN));
        cards.add(new Card(2, Card.CardState.HIDDEN));
        cards.add(new Card(3, Card.CardState.HIDDEN));
        cards.add(new Card(2, Card.CardState.HIDDEN));
        cards.add(new Card(1, Card.CardState.HIDDEN));
        cards.add(new Card(3, Card.CardState.HIDDEN));
        HashMap<Integer, Integer> pairs = game.generatePairs(cards);
        game.setCards(cards);
        game.setPairs(pairs);
        return game;
    }

    @Before
    public void setup() {
        Game game = setUpGame();
        Player player = new Player();
        gamePresenter = new GamePresenter(this.view, game, player);
    }

    @Test
    public void cardIsSelectedAndStateChangedWhenSelected() {
        gamePresenter.onCardSelected(0);
        assert(gamePresenter.getGame().getCards().get(0).getCardState() == Card.CardState.REVEALED);
        verify(view).cardSelected();
        assert(gamePresenter.getSelectedCardIndexes().contains(0));
    }

    @Test
    public void playerScoreUpdatesProperlyWhenMatchFound() {
        int initialScore = gamePresenter.getPlayerScore();
        gamePresenter.checkForMatch(0, 4);

        assert(gamePresenter.getPlayerScore()  == Game.PLAYER_WIN_MODIFIER + initialScore);
        verify(view).updateBoard();
        assert(gamePresenter.getSelectedCardIndexes().isEmpty());
    }

    @Test
    public void playerScoreUpdatesProperlyWhenMatchNotFound() {
        int initialScore = gamePresenter.getPlayerScore();
        gamePresenter.checkForMatch(0, 1);

        assert(gamePresenter.getPlayerScore()  == Game.PLAYER_LOSS_MODIFIER + initialScore);
        verify(view).updateBoard();
        assert(gamePresenter.getSelectedCardIndexes().isEmpty());
    }

    @Test
    public void gameEndsWhenAllPairsMatched() {
        gamePresenter.checkForMatch(0, 4);
        gamePresenter.checkForMatch(1, 3);
        gamePresenter.checkForMatch(2, 5);

        assert(gamePresenter.getPlayerScore()  == Game.PLAYER_WIN_MODIFIER*3);
        verify(view, times(3)).updateBoard();
        verify(view, times(1)).showGameFinishDialog();
        assert(gamePresenter.getSelectedCardIndexes().isEmpty());
    }
}
