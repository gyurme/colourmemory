package gyurme.colourmemory.Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import gyurme.colourmemory.Model.Card.CardState;

import static gyurme.colourmemory.Model.Card.CardState.HIDDEN;
import static gyurme.colourmemory.Model.Card.CardState.MATCHED;

public class Game {

    public static int NUMBER_OF_CARD_TYPES = 8;
    public static int BOARD_ROW_SIZE = 4;
    public static int BOARD_COLUMN_SIZE = 4;
    public static int PLAYER_LOSS_MODIFIER = -1;
    public static int PLAYER_WIN_MODIFIER = 2;

    private ArrayList<Card> cards;
    private HashMap<Integer, Integer> pairs;

    public Game() {
        startNewGame();
    }

    public void selectCard(int selectedCardIndex) {
        Card selectedCard = cards.get(selectedCardIndex);
        if (selectedCard.getCardState() == Card.CardState.HIDDEN) {
            selectedCard.setCardState(Card.CardState.REVEALED);
        }
    }

    public boolean matchCards(int firstCardIndex, int secondCardIndex) {
        Card firstCard = cards.get(firstCardIndex);
        Card secondCard = cards.get(secondCardIndex);

        if (pairs.get(firstCardIndex) == secondCardIndex) {
            firstCard.setCardState(MATCHED);
            secondCard.setCardState(MATCHED);
            pairs.remove(firstCardIndex);
            pairs.remove(secondCardIndex);
            return true;
        } else {
            firstCard.setCardState(CardState.HIDDEN);
            secondCard.setCardState(CardState.HIDDEN);
            return false;
        }
    }

    public boolean hasGameEnded() {
        return pairs.isEmpty();
    }

    public void startNewGame() {
        this.cards = generateCards(NUMBER_OF_CARD_TYPES);
    }

    public ArrayList<Card> generateCards(int cardTypes) {
        ArrayList<Card> cards = new ArrayList<>();
        this.pairs = new HashMap<>();

        for (int i = 0; i < 2; i++) {
            for (int j = 1; j <= cardTypes; j++) {
                Card card = new Card(j, HIDDEN);
                cards.add(card);
            }
        }

        Collections.shuffle(cards);
        this.pairs = generatePairs(cards);
        return cards;
    }

    public HashMap<Integer, Integer> generatePairs(ArrayList<Card> cards) {
        HashMap<Integer, Integer> cardPairs = new HashMap<>();

        for (int i = 0; i < cards.size() - 1; i++) {
            if (cardPairs.containsKey(i)) {
                continue;
            }
            for (int j = i + 1; j < cards.size(); j++) {
                if (cards.get(i).getId() == cards.get(j).getId()) {
                    cardPairs.put(i, j);
                    cardPairs.put(j, i);
                    break;
                }
            }
        }
        return cardPairs;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }

    public HashMap<Integer, Integer> getPairs() {
        return pairs;
    }

    public void setPairs(HashMap<Integer, Integer> pairs) {
        this.pairs = pairs;
    }
}
