package gyurme.colourmemory.Model;

public class Player {
    private String id;
    private int score;

    public Player(){
        this.score = 0;
    }

    public void modifyScore(int points) {
        this.score += points;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
