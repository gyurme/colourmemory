package gyurme.colourmemory.Model;

import android.support.annotation.NonNull;

public class Card {

    public enum CardState {
        HIDDEN,
        REVEALED,
        MATCHED
    }

    private int id;

    private CardState cardState;

    public Card(int id, CardState cardState) {
        this.id = id;
        this.cardState = cardState;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public CardState getCardState() {
        return this.cardState;
    }

    public void setCardState(@NonNull CardState cardState) {
        this.cardState = cardState;
    }
}
