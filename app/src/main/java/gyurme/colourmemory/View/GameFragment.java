package gyurme.colourmemory.View;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gyurme.colourmemory.Adapter.CardAdapter;
import gyurme.colourmemory.Database.DBUtil;
import gyurme.colourmemory.Database.HighScoreDatabase;
import gyurme.colourmemory.Presenter.GamePresenter;
import gyurme.colourmemory.R;

public class GameFragment extends Fragment implements GameView {

    @BindView(R.id.grid_view)
    GridView gridView;

    @BindView(R.id.player_score_text_view)
    TextView playerScoreTextView;

    @BindView(R.id.high_score_button)
    Button highScoreButton;

    private GamePresenter gamePresenter;

    private CardAdapter cardAdapter;

    private FragmentListener fragmentListener;

    private HighScoreDatabase highScoreDatabase;

    public GameFragment() {
    }

    //lifecycle methods

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        highScoreDatabase = DBUtil.getInstance(context.getApplicationContext()).getHighScoreDatabase();

        if (context instanceof FragmentListener) {
            fragmentListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gamePresenter = new GamePresenter(this);
        gamePresenter.setHighScoreDatabase(highScoreDatabase);
        cardAdapter = new CardAdapter(getActivity(), R.layout.card_view, gamePresenter.getGame().getCards());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game, container, false);
        ButterKnife.bind(this, view);
        gridView.setAdapter(cardAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                gamePresenter.onCardSelected(position);
            }
        });
        updateView();
        return view;
    }

    public void updateView() {
        cardAdapter.notifyDataSetChanged();
        playerScoreTextView.setText(getString(R.string.score_value, String.valueOf(gamePresenter.getPlayerScore())));
    }

    @OnClick(R.id.high_score_button)
    public void showHighScores() {
        goToHighScoreScreen();
    }

    public void cardSelected() {
        updateView();
    }

    public void updateBoard() {
        updateView();
    }

    public void resetBoard() {
        cardAdapter = new CardAdapter(getActivity(), R.layout.card_view, gamePresenter.getGame().getCards());
        gridView.setAdapter(cardAdapter);
        updateView();
    }

    public void showGameFinishDialog() {
        if (getContext() == null)
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.enter_name));
        View view = LayoutInflater.from(getContext()).inflate(R.layout.score_input_view, (ViewGroup) getView(), false);
        final TextInputLayout textInputLayout = view.findViewById(R.id.score_entry_text_input_layout);
        final TextInputEditText input = view.findViewById(R.id.score_entry_edit_text);
        builder.setView(view);
        builder.setPositiveButton(android.R.string.ok, null);
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (input.getText().toString().isEmpty()) {
                            textInputLayout.setError(getString(R.string.user_score_blank_input_error));
                            textInputLayout.setErrorEnabled(true);
                        } else {
                            gamePresenter.recordPlayerScore(input.getText().toString(), gamePresenter.getPlayerScore());
                            gamePresenter.showPlayerScore();
                            dialog.dismiss();
                        }
                    }
                });
            }
        });

        dialog.show();
    }

    public void showFinalScoreDialog() {
        if (getContext() == null)
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.score));
        builder.setMessage(getString(R.string.your_score_was) + gamePresenter.getPlayerScore());

        builder.setPositiveButton(R.string.view_high_scores, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                gamePresenter.resetGame();
                gamePresenter.showHighScores();
            }
        });

        builder.setNegativeButton(R.string.start_new_game, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                gamePresenter.resetGame();
                dialog.dismiss();
            }
        });

        builder.show();
    }

    public void goToHighScoreScreen() {
        fragmentListener.showHighScores();
    }

    public interface FragmentListener {
        void showHighScores();
    }
}
