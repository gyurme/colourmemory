package gyurme.colourmemory.View;

public interface GameView {
    void cardSelected();
    void updateBoard();
    void resetBoard();
    void showGameFinishDialog();
    void showFinalScoreDialog();
    void goToHighScoreScreen();
}
