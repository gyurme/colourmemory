package gyurme.colourmemory;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import gyurme.colourmemory.Adapter.ScoreAdapter;
import gyurme.colourmemory.Database.HighScore;
import gyurme.colourmemory.Database.HighScoreDatabase;
import gyurme.colourmemory.R;

public class ScoreFragment extends Fragment {
    private static final String DATABASE_NAME = "highScoreDatabase";

    private HighScoreDatabase highScoreDatabase;

    private ArrayList<HighScore> scores;

    @BindView(R.id.score_recycler_view)
    RecyclerView recyclerView;

    ScoreAdapter scoreAdapter;

    public ScoreFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_score, container, false);
        ButterKnife.bind(this, view);
        scores = new ArrayList<>();
        scoreAdapter = new ScoreAdapter(scores);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(scoreAdapter);
        populateScores();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.highScoreDatabase = Room.databaseBuilder(context.getApplicationContext(),
                HighScoreDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void populateScores() {
        scores.clear();
        List<HighScore> stored = highScoreDatabase.getHighScorDao().getAllScores();
        scores.addAll(stored);
        scoreAdapter.notifyDataSetChanged();
    }
}
