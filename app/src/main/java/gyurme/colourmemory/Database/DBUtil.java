package gyurme.colourmemory.Database;

import android.arch.persistence.room.Room;
import android.content.Context;

/**
 * Singleton Helper class used to initialize and return a database instance
 */
public class DBUtil {

    private static final String DATABASE_NAME = "highScoreDatabase";
    private static DBUtil dBUtilInstance;
    private HighScoreDatabase highScoreDatabase;

    private DBUtil(Context context) {
        if (dBUtilInstance != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }

        this.highScoreDatabase = Room.databaseBuilder(context.getApplicationContext(),
                HighScoreDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }

    public static DBUtil getInstance(Context context) {
        if (dBUtilInstance == null) {
            synchronized (DBUtil.class) {
                if (dBUtilInstance == null) dBUtilInstance = new DBUtil(context);
            }
        }
        return dBUtilInstance;
    }

    public HighScoreDatabase getHighScoreDatabase() {
        return highScoreDatabase;
    }
}
