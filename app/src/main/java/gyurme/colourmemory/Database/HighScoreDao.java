package gyurme.colourmemory.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface HighScoreDao {

    @Query("SELECT * FROM highScores ORDER BY score DESC")
    List<HighScore> getAllScores();

    @Insert
    void insert(HighScore... highScores);

    @Update
    void update(HighScore... highScores);

    @Delete
    void delete(HighScore... highScores);
}

