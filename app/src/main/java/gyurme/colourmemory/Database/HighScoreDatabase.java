package gyurme.colourmemory.Database;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {HighScore.class}, version = 1)
public abstract class HighScoreDatabase extends RoomDatabase {

    private static final String DB_NAME = "highScoreDatabase.db";
    private static volatile HighScoreDatabase instance;

    static synchronized HighScoreDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static HighScoreDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                HighScoreDatabase.class,
                DB_NAME).build();
    }

    public abstract HighScoreDao getHighScorDao();
}
