package gyurme.colourmemory.Presenter;

import android.os.CountDownTimer;

import java.util.ArrayList;

import gyurme.colourmemory.Database.HighScore;
import gyurme.colourmemory.Database.HighScoreDatabase;
import gyurme.colourmemory.Model.Card;
import gyurme.colourmemory.Model.Game;
import gyurme.colourmemory.Model.Player;
import gyurme.colourmemory.View.GameView;


public class GamePresenter {

    private Game game;

    private Player player;

    private GameView view;

    private ArrayList<Integer> selectedCardIndexes;

    private boolean delayActive;

    private HighScoreDatabase highScoreDatabase;

    public GamePresenter(GameView view) {
        this.view = view;
        this.game = new Game();
        this.player = new Player();
        this.selectedCardIndexes = new ArrayList<>();
    }

    public GamePresenter(GameView view, Game game, Player player) {
        this.view = view;
        this.game = game;
        this.player = player;
        this.selectedCardIndexes = new ArrayList<>();;
    }

    public void onCardSelected(int cardIndex) {
        //ignore selections on cards during the delay, or if the card is already selected or matched.
        if (delayActive || selectedCardIndexes.contains(cardIndex) || game.getCards().get(cardIndex).getCardState() == Card.CardState.MATCHED) {
            return;
        }
        game.selectCard(cardIndex);
        view.cardSelected();

        selectedCardIndexes.add(cardIndex);

        if (selectedCardIndexes.size() == 2) {
            //delay checking for a match so we can see the last selected card for a second.
            delayThenCheckForMatch();
        }
    }

    private void delayThenCheckForMatch() {
        delayActive = true;

        new CountDownTimer(1000, 1000) {
            public void onFinish() {
                checkForMatch(selectedCardIndexes.get(0), selectedCardIndexes.get(1));
            }
            public void onTick(long millisUntilFinished) {
                //required
            }
        }.start();
    }

    public void checkForMatch(int firstCardIndex, int secondCardIndex) {
        boolean matchFound = game.matchCards(firstCardIndex, secondCardIndex);
        if (matchFound) {
            player.modifyScore(Game.PLAYER_WIN_MODIFIER);
        } else {
            player.modifyScore(Game.PLAYER_LOSS_MODIFIER);
        }
        selectedCardIndexes.clear();
        delayActive = false;
        view.updateBoard();
        if (game.hasGameEnded()) {
            finishGame();
        }
    }

    public void finishGame() {
        view.showGameFinishDialog();
    }

    public void resetGame() {
        player = new Player();
        game = new Game();
        selectedCardIndexes.clear();
        view.resetBoard();
    }

    public void recordPlayerScore(final String playerName, final int playerScore) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                HighScore highScore = new HighScore(playerName, playerScore);
                highScoreDatabase.getHighScorDao().insert(highScore);
            }
        }).start();
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void showPlayerScore() {
        view.showFinalScoreDialog();
    }

    public void showHighScores() {
        this.view.goToHighScoreScreen();
    }

    public int getPlayerScore() {
        return player.getScore();
    }

    public void setPlayerScore(int score) {
        player.setScore(score);
    }

    public ArrayList<Integer> getSelectedCardIndexes() {
        return selectedCardIndexes;
    }

    public void setSelectedCardIndexes(ArrayList<Integer> selectedCardIndexes) {
        this.selectedCardIndexes = selectedCardIndexes;
    }

    public HighScoreDatabase getHighScoreDatabase() {
        return highScoreDatabase;
    }

    public void setHighScoreDatabase(HighScoreDatabase highScoreDatabase) {
        this.highScoreDatabase = highScoreDatabase;
    }
}
