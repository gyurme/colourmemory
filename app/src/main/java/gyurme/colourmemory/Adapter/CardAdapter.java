package gyurme.colourmemory.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import gyurme.colourmemory.Model.Card;
import gyurme.colourmemory.Model.Game;
import gyurme.colourmemory.R;

public class CardAdapter extends ArrayAdapter<Card> {

    public CardAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Card> cards) {
        super(context, resource, cards);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.card_view, null);
        }

        Card card = getItem(position);

        if (card == null)
            return convertView;

        DisplayMetrics displayMetrics = convertView.getResources().getDisplayMetrics();
        int screen_width = displayMetrics.widthPixels;    //width of the device screen
        int screen_height = displayMetrics.heightPixels;   //height of device screen
        int view_width = screen_width / Game.BOARD_COLUMN_SIZE;   // width for imageview
        int view_height = screen_height / (Game.BOARD_ROW_SIZE + 1);//height for imageview with an offset for the header

        ImageView imageView = convertView.findViewById(R.id.card_imageView);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.getLayoutParams().width = view_width;
        imageView.getLayoutParams().height = view_height;

        int drawableID;
        Card.CardState cardState = card.getCardState();
        switch (card.getCardState()) {
            case HIDDEN:
                drawableID = R.drawable.card_back;
                imageView.setImageResource(drawableID);
                break;
            case REVEALED:
                drawableID = convertView.getResources().getIdentifier(
                        "colour" + getItem(position).getId(), "drawable", "gyurme.colourmemory");
                imageView.setImageResource(drawableID);
                break;
            case MATCHED:
                imageView.setImageBitmap(null);
                break;
        }
        return convertView;
    }
}

