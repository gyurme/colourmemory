package gyurme.colourmemory.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import gyurme.colourmemory.Database.HighScore;
import gyurme.colourmemory.R;

public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.ScoreViewHolder> {

    public static final int RANK_OFFSET = 1;

    private List<HighScore> highScores;

    public ScoreAdapter(List<HighScore> highScores) {
        this.highScores = highScores;
    }

    @Override
    public ScoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_score, parent, false);

        return new ScoreViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ScoreViewHolder holder, int position) {
        HighScore score = highScores.get(position);
        holder.playerRank.setText(String.valueOf(position + RANK_OFFSET));
        holder.playerName.setText(score.getName());
        holder.playerScore.setText(String.valueOf(score.getScore()));
    }

    @Override
    public int getItemCount() {
        return highScores.size();
    }

    public class ScoreViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.player_rank)
        TextView playerRank;
        @BindView(R.id.player_name)
        TextView playerName;
        @BindView(R.id.player_score)
        TextView playerScore;

        public ScoreViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
