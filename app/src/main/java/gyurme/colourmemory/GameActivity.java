package gyurme.colourmemory;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.List;

import gyurme.colourmemory.Database.HighScore;
import gyurme.colourmemory.View.GameFragment;

public class GameActivity extends AppCompatActivity implements GameFragment.FragmentListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        FragmentManager fm = getSupportFragmentManager();
        GameFragment fragment = (GameFragment) fm.findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = new GameFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
    }

    @Override
    public void showHighScores() {
        Intent intent = new Intent(this, ScoreActivity.class);
        startActivity(intent);
    }
}
